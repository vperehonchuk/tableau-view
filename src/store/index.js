import { Vue } from 'vue-property-decorator';
import Vuex, { Store } from 'vuex';
import tableau from './tableau';

Vue.use(Vuex);

export default new Store({
  getters: {
    error(state) {
      return state.tableau.error;
    },
    isLoading(state) {
      return !!state.tableau.loading;
    },
  },
  modules: { tableau },
  strict: process.env.NODE_ENV !== 'production',
});
