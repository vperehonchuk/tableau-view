import childProcess from 'child_process';

childProcess.execSync('vue-cli-service build');
childProcess.execSync(
  'babel --config-file ./.server.babelrc src/index.js --out-file dist/index.js',
);
childProcess.execSync(
  'babel --config-file ./.server.babelrc src/server.js --out-file dist/server.js',
);
